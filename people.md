# Census & People

Town Population: 300

## Town Members [Players]

| Name                  | Race                | Class       | Spec         | Town Role        | Player   | Profile  |
|-----------------------|---------------------|-------------|--------------|------------------|----------|----------|
| Kai Curacao           | Pianta (Solar)      | Bard        | Eloquence    | Brewer, Merchant | Kenny    | ![Kai](/images/profile-kai.png "Profile") |
| Annabelle Watkins     | Human               | Ranger      | Drakewarden  | Farmer           | Anne     | ![Annabelle](/images/profile-annabelle.png "Profile") |
| Malikai               | Tiefling            | Bloodhunter |              | Field Alchemist  | Michael  | ![Malikai](/images/profile-malikai.png "Profile") |
| Cornelius Pip         | Owlin               | Wizard      |              | Alchemist        | Taylor   | ![Cornelius](/images/profile-cornelius.png "Profile") |
| Ramos Asmadi          | Dragonborn (Silver) | Paladin     | Conquest     | Cartographer     | Morgan   | ![Ramos](/images/profile-ramos.png "Profile") |
| Myki Rhyza            | Pianta (Lunar)      | Druid       | Moon         | Miner, Innkeeper | Cameron  | ![Myki](/images/profile-myki.png "Profile") |
| Aleksandra MacCaslin  | Dwarf (Mountain)    | Artificer   | Battle Smith | Miner, Blacksmith| Caroline | ![Aleksandra](/images/profile-aleksandra.png "Profile") |
| Five-Timber           | Tabaxi              | Rogue       |              | Hunter           | Jamie    | ![Fivetimber](/images/profile-fivetimber.png "Profile") |
| Sir William Helm | Human           |             |              | Factor           | Noah     | ![Sir Edmond](/images/profile-edmond.png "Profile") |

## Town Members [NPC]

TODO
