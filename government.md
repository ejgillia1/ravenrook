# Government Information

## Officals
- Factor: Sir Wlliam Helm
- Assistant to the Factor: Henry Johnson

## Taxes
- All citizens and companies inside colonial territory must pay a 4% income tax to support the development of the settlement
- Tax collections go to support the pay of workers, acquisition of raw materials, and fees imposed on the company by the country of incorporation


## Colony Coffers

TBD

## Town Storage

TODO

## Debts & Expectations
- Mining Quota (TBD)
